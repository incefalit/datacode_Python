import os

from django.http import HttpResponse
from ppf.datamatrix import DataMatrix

def index(request):
    codeString = request.GET.get("code")
    path = codeGenerator(codeString)
    responce = (f"<h2>Введите код</h2><form><input type=\"text\" id=\"code\" name=\"code\"></form><br>{path}")
    return HttpResponse(responce)

def codeGenerator(codeString):
    result = DataMatrix(codeString)
    with open("code.svg", "w") as f:
        f.write(result.svg())
    return os.path.abspath('code.svg')