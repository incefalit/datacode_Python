# datacode

Download and install Python
https://www.python.org/downloads/

Go to C:\ and init new project in console
* Windows:
`py -m venv ProjectName`
* Unix/MacOS:
`python -m venv ProjectName`

Go to the project folder:
`cd c:\ProjectName`

Then in console:
1. * Windows:
`Scripts\activate.bat`
   * Unix/MacOS:
   `source bin/activate`
2. * Windows:
`(ProjectName) C:\ProjectName>py -m pip install Django`
   * Unix/MacOS:
   `(ProjectName) ... $ python -m pip install Django`
3. * Windows:
`(ProjectName) C:\ProjectName>py -m pip install ppf.datamatrix`
   * Unix/MacOS:
   `(ProjectName) ... $ python -m pip install ppf.datamatrix`

From repository
https://gitlab.com/incefalit/datacode_Python.git
copy **data_code** folder and put it in the project folder

run server
`py data_code\manage.py runserver`

Startup link is
http://127.0.0.1:8000/?code=0
